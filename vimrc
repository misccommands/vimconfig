set encoding=UTF-8

set autoread
au CursorHold * checktime  "detect file change

set hlsearch               "Highlight search results
set incsearch              "Set incremental search

"set+=thesaurus.txt         "<ctrl-x><ctrl-t> in insert mode.

"set correct indenting for gg=G & \cf for omnisharp
set expandtab
set shiftwidth=3
set tabstop=3
"set autoindent
"set smartindent
"set cindent

"set noshowmode "hide --INSERT--

"### GUI Options
"set guioptions -=T "hide gVim toolbar
"set guioptions=  "hide all options
"set guioptions -=r "hide scrollbar
"set guioptions =M
function! ToggleGUICruft()
   if &guioptions=='i'
      exec('set guioptions=imTrL')
   else
      exec('set guioptions=i')
   endif
endfunction

"Set F11 to to toggle Toolbar display
map <F11> <Esc>:call ToggleGUICruft()<cr>

"hide gui scrollbar & toolbar
set guioptions=i

"### Font Options
"set guifont=SauceCodePro\ Nerd\ Font\ Mono\ 12 "set fonts for NerdTree
"set guifont=SauceCodePro\ Nerd\ Font\ 12 "set fonts for NerdTree
"set guifont=DroidSansMono\ Nerd\ Font\ 12 "set fonts for NerdTree
"set guifont=UbuntuMono\ Nerd\ Font\ Mono\ 13 "set fonts for NerdTree
"set guifont=RobotoMono\ Nerd\ Font\ 13 "set fonts for NerdTree
"set guifont=FiraCode\ Nerd\ Font\ Mono\ 12 "set fonts for NerdTree
"###

if has("gui_running")
   if has("gui_gtk2")
      "set guifont=Consolas\ 14
      set guifont=UbuntuMono\ Nerd\ Font\ Mono\ 13 "set fonts for Airline or NerdTree
      "elseif has("gui_macvim")
      "set guifont=Menlo\ Regular:h14
   elseif has("gui_win32")
      "set guifont=Consolas:h11:cANSI
      set guifont=UbuntuMono\NF :h13:cANSI
   endif
endif


"tab completion when opening files
set wildmode=longest,list,full
set wildmenu

syntax enable
set number
filetype indent on

set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'OmniSharp/omnisharp-vim'
Plugin 'tpope/vim-dispatch'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'prabirshrestha/asyncomplete.vim'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets' "snippets for utilsnips
"Plugin 'ycm-core/YouCompleteMe' "Show Snippets in Dropdown
Plugin 'dense-analysis/ale' "Asynchronous Lint Engine
Plugin 'scrooloose/nerdcommenter' "[count]<leader> c <space>, cy (comment and yank)
Plugin 'airblade/vim-gitgutter'
"Plugin 'itchyny/lightline.vim'
Plugin 'vim-airline/vim-airline' "buffer line
Plugin 'vim-airline/vim-airline-themes'
"Plugin 'ryanoasis/vim-devicons' "Nerdtree icons
"Plugin 'mattn/emmet-vim'               "html completion.. ie html:5 (c-y ,)
"Plugin 'junegunn/fzf.vim'              "test in place of CtrlP
"Plugin 'terryma/vim-multiple-cursors'  "multiple cursors
"Plugin 'tpope/vim-eunuch'              "adds commands to Vim for better workflow
"
"Color Schemes
Plugin 'gilgigilgil/anderson.vim'
Plugin 'morhetz/gruvbox'
Plugin 'altercation/vim-colors-solarized'
Plugin 'tomasr/molokai'
Plugin 'dracula/vim', {'name':'dracula'}
Plugin 'arcticicestudio/nord-vim'
Plugin 'jnurmine/Zenburn'
Plugin 'kyoz/purify', {'rtp':'vim'}
Plugin 'NLKNguyen/papercolor-theme'
Plugin 'nanotech/jellybeans.vim'
Plugin 'lifepillar/vim-solarized8'

call vundle#end()

let g:airline#extensions#tabline#enabled = 1

"https://github.com/vim-airline/vim-airline-themes/blob/master/doc/airline-themes.txt
"let g:airline_theme='light'
"let g:airline_theme='sol'
"let g:airline_theme='silver'
let g:airline_theme='papercolor'
"let g:airline_theme='bubblegum'
"let g:airline_solarized_bg='dark'
" Show just the filename
"let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline_powerline_fonts = 1

"enable snippets " <c-tab> to list snippets
let g:OmniSharp_want_snippet=1 "use a + and then tab, ie. ps+ <tab> 

"Snippet Trigger configuration. 
let g:UltiSnipsListSnippets ="<c-tab>"
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"

"Set Ctrp P
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

set background=light
"colorscheme purify
"colorscheme zenburn
"colorscheme PaperColor 
"colorscheme jellybeans 
"colorscheme nord
"colorscheme dracula
"colorscheme gruvbox
"colorscheme anderson
colorscheme solarized8_high   "high-contrast variant 
"colorscheme solarized8        "the default Solarized theme
"colorshceme solarized8_low    "low-contrast variant
"colorscheme  solarized8_flat   "flat variant



let g:gruvbox_contrast_dark='soft'   "soft, medium, hard

"open gvim to the correct size
if has("gui_running")
   set lines=58 columns=156
endif

syntax on
filetype plugin indent on

" ctrlp
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:NERDTreeIgnore = ['^node_modules$']

" Triger `autoread` when files changes on disk
autocmd CursorHold,CursorHoldI * call NERDTreeFocus() | call g:NERDTree.ForCurrentTab().getRoot().refresh() | call g:NERDTree.ForCurrentTab().render() | wincmd w

"open NerdTree on empty files
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

"open directory
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

"Buffers
set hidden

function! IsNERDTreeOpen()        
   return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction

function! NextBuffer()
   bnext
   if IsNERDTreeOpen() 
      NERDTreeFind
      wincmd p
   endif
endfunction

execute "set <M-p>=\ep"
nnoremap <M-p> <Esc>:call NextBuffer()<CR>

function! PrevBuffer()
   bprev
   if IsNERDTreeOpen() 
      NERDTreeFind
      wincmd p
   endif
endfunction

execute "set <M-o>=\eo"
nnoremap <M-o> <Esc>:call PrevBuffer()<CR>

function! ToggleNT()
   NERDTreeToggle
endfunction

execute "set <M-f>=\ef"
map <M-f> <Esc>:call ToggleNT()<CR>

"NERDTree highlights current buffer
"function! SyncTree()
      "if &modifiable && IsNERDTreeOpen() && strlen(expand('%')) > 0 && !&diff
      "NERDTreeFind
      "wincmd p
   "endif
"endfunction

" Highlight currently open buffer in NERDTree
"autocmd BufEnter * call SyncTree()

"close Vim=>q, close Buffer=>bd if only nerdtree is open when closing a
"file/buffer
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | bd | endif

"NerdTree Arrows
"let g:NERDTreeDirArrowExpandable = '▸'
"let g:NERDTreeDirArrowCollapsible = '▾'

"if has("gui_running")
"if has("gui_gtk2")
"set guifont=Consolas\ 14
"elseif has("gui_macvim")
"set guifont=Menlo\ Regular:h14
"elseif has("gui_win32")
"set guifont=Consolas:h11:cANSI
"endif
"endif

" Use the stdio OmniSharp-roslyn server
let g:OmniSharp_server_stdio = 1

" Set the type lookup function to use the preview window instead of echoing it
"let g:OmniSharp_typeLookupInPreview = 1
" Timeout in seconds to wait for a response from the server
let g:OmniSharp_timeout = 5


" Don't autoselect first omnicomplete option, show options even if there is only
" one (so the preview documentation is accessible). Remove 'preview' if you
" don't want to see any documentation whatsoever.
set completeopt=longest,menuone,preview

" Fetch full documentation during omnicomplete requests.
" By default, only Type/Method signatures are fetched. Full documentation can
" still be fetched when you need it with the :OmniSharpDocumentation command.

"let g:omnicomplete_fetch_full_documentation = 1

" Set desired preview window height for viewing documentation.
" You might also want to look at the echodoc plugin.

set previewheight=5

" Tell ALE to use OmniSharp for linting C# files, and no other linters.
let g:ale_linters = { 'cs': ['OmniSharp'] }

" Update semantic highlighting after all text changes
let g:OmniSharp_highlight_types = 3

" Update semantic highlighting on BufEnter and InsertLeave
 let g:OmniSharp_highlight_types = 2

augroup omnisharp_commands
   autocmd!

   " Show type information automatically when the cursor stops moving
   autocmd CursorHold *.cs call OmniSharp#TypeLookupWithoutDocumentation()

   " The following commands are contextual, based on the cursor position.
   autocmd FileType cs nnoremap <buffer> gd :OmniSharpGotoDefinition<CR>
   autocmd FileType cs nnoremap <buffer> <Leader>fi :OmniSharpFindImplementations<CR>
   autocmd FileType cs nnoremap <buffer> <Leader>fs :OmniSharpFindSymbol<CR>
   autocmd FileType cs nnoremap <buffer> <Leader>fu :OmniSharpFindUsages<CR>

   " Finds members in the current buffer
   autocmd FileType cs nnoremap <buffer> <Leader>fm :OmniSharpFindMembers<CR>
   autocmd FileType cs nnoremap <buffer> <Leader>fx :OmniSharpFixUsings<CR>
   autocmd FileType cs nnoremap <buffer> <Leader>tt :OmniSharpTypeLookup<CR>
   autocmd FileType cs nnoremap <buffer> <Leader>dc :OmniSharpDocumentation<CR>
   autocmd FileType cs nnoremap <buffer> <C-\> :OmniSharpSignatureHelp<CR>
   autocmd FileType cs inoremap <buffer> <C-\> <C-o>:OmniSharpSignatureHelp<CR>

   " Navigate up and down by method/property/field
   autocmd FileType cs nnoremap <buffer> <C-k> :OmniSharpNavigateUp<CR>
   autocmd FileType cs nnoremap <buffer> <C-j> :OmniSharpNavigateDown<CR>

   " Find all code errors/warnings for the current solution and populate the quickfix window
   autocmd FileType cs nnoremap <buffer> <Leader>cc :OmniSharpGlobalCodeCheck<CR>
augroup END

" Contextual code actions (uses fzf, CtrlP or unite.vim when available)
nnoremap <Leader><Space> :OmniSharpGetCodeActions<CR>

" Run code actions with text selected in visual mode to extract method
xnoremap <Leader><Space> :call OmniSharp#GetCodeActions('visual')<CR>

" Rename with dialog
nnoremap <Leader>nm :OmniSharpRename<CR>
nnoremap <F2> :OmniSharpRename<CR>

" Rename without dialog - with cursor on the symbol to rename: `:Rename newname`
command! -nargs=1 Rename :call OmniSharp#RenameTo("<args>")
nnoremap <Leader>cf :OmniSharpCodeFormat<CR>

" Start the omnisharp server for the current solution
nnoremap <Leader>ss :OmniSharpStartServer<CR>
nnoremap <Leader>sp :OmniSharpStopServer<CR>
